//
//  SoundController.h
//  MacSoundPlay
//
//  Created by kaydell on 12/11/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoundModel.h"
#import "SoundView.h"

@class SoundModel, SoundView;

@interface SoundController : NSObject
{
    @private
    IBOutlet SoundModel *soundModel;
    IBOutlet SoundView *soundView;
}
- (void)setSoundModel:(SoundModel *)newSoundModel;
- (void)play;
@end
