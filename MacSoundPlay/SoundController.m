//
//  SoundController.m
//  MacSoundPlay
//
//  Created by kaydell on 12/11/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import "SoundController.h"

@implementation SoundController

- (void)play
{
    [soundModel play];
}

- (void)setSoundModel:(SoundModel *)newSoundModel
{
    soundModel = newSoundModel;
}

@end
