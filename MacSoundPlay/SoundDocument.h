//
//  Document.h
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SoundModel.h"
#import "SoundView.h"

/**
 A forward declaration because SoundDocument uses SoundView
 and vice verse.
 */
@class SoundView;

@interface SoundDocument : NSDocument
{
    @private
    SoundModel *soundModel;
    IBOutlet SoundView *soundView;          // the view of model-view-controller
    IBOutlet SoundController *soundController;   // the controller of model-view-controller
}
@end
