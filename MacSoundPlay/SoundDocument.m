//
//  Document.m
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import "SoundDocument.h"
#import "Utils.h"

@implementation SoundDocument

// init just does the standard stuff and inits all instance variables
// of this class.
- (id)init
{
    self = [super init];
    if (self) {
        soundModel = nil;
        soundView = nil;
        soundController = nil;
    }
    return self;
}

- (NSString *)windowNibName
{
    return @"SoundDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aWindowController
{
    [super windowControllerDidLoadNib:aWindowController];
    [soundController setSoundModel:soundModel];
    soundModel = nil;
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

// called by save
// Not really used because this app is not an "editor" only a "viewer"
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    *outError = nil;
    return nil;
}

// called by open
- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    soundModel = [[SoundModel alloc] initWithData:data]; // create a soundModel object
    *outError = nil;
    return YES;
}

@end
