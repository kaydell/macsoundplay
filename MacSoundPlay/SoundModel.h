//
//  SoundModel.h
//  MacSoundPlay
//
//  Created by kaydell on 12/11/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundModel : NSObject
{
    @private
    NSSound *sound;
}
- (id)initWithData:(NSData *)data;
- (void)play;
@end
