//
//  SoundModel.m
//  MacSoundPlay
//
//  Created by kaydell on 12/11/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import "SoundModel.h"

@implementation SoundModel

- (id)initWithData:(NSData *)data {
    self = [super init];
    if (self) {
        sound = [[NSSound alloc] initWithData:data];
        [sound setLoops:YES];                        // auto loop sound
    }
    return self;
}

- (void)play 
{
    [sound play]; 
}

@end
