//
//  SongView.h
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SoundController.h"

@class SoundController;

/**
 This class is the view of the model-view-controller (MVC) design pattern.
 */
@interface SoundView : NSWindow
{
    @private
    IBOutlet SoundController *soundController;
}
@end
