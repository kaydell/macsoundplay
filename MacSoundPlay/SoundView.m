//
//  SongView.m
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import "SoundView.h"

@implementation SoundView

/**
 This method passes on the message to the soundDocument object
 to actually play its sound.
 */
- (IBAction)play:(id)sender
{
    [soundController play];
}

@end
