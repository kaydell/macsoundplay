//
//  Utils.h
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 This unit implements generally useful methods
 */
@interface Utils : NSObject
+ (void)displayAlert:(NSString *)message;
@end
