//
//  Utils.m
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import "Utils.h"

@implementation Utils

/**
 This method is used to easily display a simple alert window
 with a message and an OK button
 */
+ (void)displayAlert:(NSString *)message
{
    // require that message not be nil
    if (!message) {
        message = @"Programming Error: message cannot be nil";
        // TODO: NSLog(message);
        [self displayAlert:message];
        return;
    }
    // otherwise, display an alert
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:message];
    [alert runModal];
}

@end
