//
//  main.m
//  MacSoundPlay
//
//  Created by kaydell on 11/4/13.
//  Copyright (c) 2013 Kaydell Leavitt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
